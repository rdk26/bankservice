package ru.superbank.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.superbank.exceptions.IncorrectDataException;
import ru.superbank.exceptions.NotFoundLimitException;
import ru.superbank.model.dto.response.ErrorResponse;

import java.util.Date;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(IncorrectDataException.class)
    public ResponseEntity<ErrorResponse> notFoundByIdException(IncorrectDataException idException) {
        ErrorResponse errorResponse = new ErrorResponse(idException.getMessage(), new Date(), HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND.value());
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NotFoundLimitException.class)
    public ResponseEntity<ErrorResponse> notFoundLimitException(NotFoundLimitException idException) {
        ErrorResponse errorResponse = new ErrorResponse(idException.getMessage(), new Date(), HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND.value());
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }
}

delete from public.transaction;
delete from public.client_limit;
delete from public.conversion;
delete from public.client;

--Client init
INSERT INTO public.client (id, created, bank_account_number, is_employee)
VALUES (1, '2023-09-01T15:00:00', '0123456789', true);
INSERT INTO public.client (id, created, bank_account_number, is_employee)
VALUES (2, '2023-09-02T16:00:00', '0567845678', true);
INSERT INTO public.client (id, created, bank_account_number, is_employee)
VALUES (3, '2023-09-05T17:00:00', '0567845600', true);

--Client limit init
INSERT INTO public.client_limit (created, sum, remaining, expense_category, client_id)
VALUES ('2023-09-01T15:00:00', 3000, 5000, 'PRODUCT', 1);
INSERT INTO public.client_limit (created, sum, remaining, expense_category, client_id)
VALUES ('2023-09-02T15:00:00', 5000, 0, 'SERVICE', 1);
INSERT INTO public.client_limit (created, sum, remaining, expense_category, client_id)
VALUES ('2023-09-02T15:00:00', 4000, 3000, 'PRODUCT', 2);
INSERT INTO public.client_limit (created, sum, remaining, expense_category, client_id)
VALUES ('2023-09-02T15:00:00', 2000, 1000, 'SERVICE', 3);
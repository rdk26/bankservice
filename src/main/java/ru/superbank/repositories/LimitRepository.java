package ru.superbank.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.superbank.model.db.ClientLimit;
import ru.superbank.model.dto.ClientLimitDto;

import java.time.LocalDateTime;

@Repository
public interface LimitRepository extends JpaRepository <ClientLimit, Long> {

    ClientLimit findFirstByClientIdAndExpenseCategoryOrderByCreatedDesc(Long client_id, String expenseCategory);


    @Query(value = "select new ru.superbank.model.dto.ClientLimitDto(l.created, l.sum, l.remaining, l.expenseCategory)" +
            " from ClientLimit l where l.client.id=:clientId and (coalesce(:createdFrom, '1970-01-01 00:00:00')='1970-01-01 00:00:00' or l.created >=:createdFrom)" +
            " and (coalesce(:createdTo, '1970-01-01 00:00:00')='1970-01-01 00:00:00' or l.created <=:createdTo) order by l.created asc ")
    Page<ClientLimitDto> findAllByClientIdAndCreatedRange(@Param("clientId") Long clientId,
                                                     @Param("createdFrom") LocalDateTime createdFrom,
                                                     @Param("createdTo") LocalDateTime createdTo, Pageable pageable);

}

package ru.superbank.mappers;

import org.mapstruct.Mapper;
import ru.superbank.model.db.Transaction;
import ru.superbank.model.dto.request.TransactionRq;

@Mapper(componentModel = "spring")
public interface TransactionMapper {
    Transaction toEntity(TransactionRq model);
}

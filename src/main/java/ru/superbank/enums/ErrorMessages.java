package ru.superbank.enums;

public enum ErrorMessages {
    INCORRECT_DATA_MESSAGE("You entered incorrect data (%s), try again or contact support"),
    EMPTY_DATA("the %s is not specified");

    private final String message;

    ErrorMessages(String m) {
        message = m;
    }

    public String getMessage() {
        return message;
    }
}

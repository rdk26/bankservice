package ru.superbank.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.superbank.model.db.Transaction;
import ru.superbank.model.dto.TransactionDto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query(value = "select new ru.superbank.model.dto.TransactionDto(t.created," +
            " t.accountFrom, t.accountTo, t.amountKZT, t.amountRUB, t.amountUSD, " +
            "t.expenseCategory, t.exceeded) from Transaction t where t.client.id=:clientId " +
            " and (coalesce(:createdFrom, '1970-01-01 00:00:00')='1970-01-01 00:00:00' or t.created >=:createdFrom)" +
            " and (coalesce(:createdTo, '1970-01-01 00:00:00')='1970-01-01 00:00:00' or t.created <=:createdTo) order by t.created asc ")
    Page<TransactionDto> findAllByClientAndCreatedRange(@Param("clientId") Long clientId,
                                                        @Param("createdFrom") LocalDateTime createdFrom,
                                                        @Param("createdTo") LocalDateTime createdTo, Pageable pageable);

    @Query(value = "select sum(t.amountUSD) from Transaction t where t.client.id=:clientId " +
                   "and t.expenseCategory=:category group by t.client")
    Optional<BigDecimal> getSumByClientIdAndExpenseCategory(@Param("clientId") Long clientId,
                                                  @Param("category") String category);
    @Query(value = "select t.* " +
            "from Transaction t join client_limit cl on cl.id = t.limit_id " +
            "join (select max(l2.created) as created, l2.expense_category " +
            "from client_limit l2 where l2.client_id=:client_id group by l2.expense_category) limit2 " +
            "on cl.expense_category=limit2.expense_category and cl.created=limit2.created " +
            "where t.exceeded=true and t.client_id=:client_id", nativeQuery = true)
    Page<Transaction> getExceededTransactionByClientId(@Param("client_id") Long clientId, Pageable pageable);
}

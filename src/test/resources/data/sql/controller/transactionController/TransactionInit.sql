delete from public.transaction;
delete from public.client_limit;
delete from public.conversion;
delete from public.client;

INSERT INTO public.client (id, created, bank_account_number, is_employee)
VALUES (1, '2023-09-01T15:00:00', '0123456789', true);
INSERT INTO public.client (id, created, bank_account_number, is_employee)
VALUES (2, '2023-09-02T16:00:00', '0567845678', true);
INSERT INTO public.client (id, created, bank_account_number, is_employee)
VALUES (3, '2023-09-05T17:00:00', '0567845600', true);

--Client limit init
INSERT INTO public.client_limit (id, created, sum, remaining, expense_category, client_id)
VALUES (1, '2023-01-01T15:00:00', 1000, 1000, 'PRODUCT', 1);
INSERT INTO public.client_limit (id, created, sum, remaining, expense_category, client_id)
VALUES (2, '2023-01-10T15:00:00', 2000, -400, 'PRODUCT', 1);
INSERT INTO public.client_limit (id, created, sum, remaining, expense_category, client_id)
VALUES (3, '2023-09-02T15:00:00', 2000, 2000, 'SERVICE', 3);

INSERT INTO public.conversion (rate, made_at, symbol)
VALUES (99.0, '2023-09-01', 'USD/RUB');
INSERT INTO public.conversion (rate, made_at, symbol)
VALUES (99.53, '2023-09-02', 'USD/RUB');
INSERT INTO public.conversion (rate, made_at, symbol)
VALUES (99.57, '2023-09-03', 'USD/RUB');
INSERT INTO public.conversion (rate, made_at, symbol)
VALUES (100.0, '2023-09-09', 'USD/RUB');

INSERT INTO public.conversion (rate, made_at, symbol)
VALUES (440.0, '2023-09-01', 'USD/KZT');
INSERT INTO public.conversion (rate, made_at, symbol)
VALUES (445.53, '2023-09-02', 'USD/KZT');
INSERT INTO public.conversion (rate, made_at, symbol)
VALUES (460.57, '2023-09-03', 'USD/KZT');
INSERT INTO public.conversion (rate, made_at, symbol)
VALUES (470.0, '2023-09-09', 'USD/KZT');

INSERT INTO public.transaction (created, account_from, account_to, amount_kzt, amount_rub, amount_usd, expense_category, exceeded, limit_id, client_id)
VALUES ('2023-01-02T15:04:00', '0123456789', '0567845678', 0, 50000, 500, 'PRODUCT', false, 1, 1);
INSERT INTO public.transaction (created, account_from, account_to, amount_kzt, amount_rub, amount_usd, expense_category, exceeded, limit_id, client_id)
VALUES ('2023-01-03T15:00:00', '0123456789', '0567845678', 0, 60000, 600, 'PRODUCT', true, 1, 1);
INSERT INTO public.transaction (created, account_from, account_to, amount_kzt, amount_rub, amount_usd, expense_category, exceeded, limit_id, client_id)
VALUES ('2023-01-11T15:00:00', '0123456789', '0567845678', 0, 10000, 100, 'PRODUCT', false, 2, 1);
INSERT INTO public.transaction (created, account_from, account_to, amount_kzt, amount_rub, amount_usd, expense_category, exceeded, limit_id, client_id)
VALUES ('2023-01-12T15:00:00', '0123456789', '0567845678', 0, 70000, 700, 'PRODUCT', false, 2, 1);
INSERT INTO public.transaction (created, account_from, account_to, amount_kzt, amount_rub, amount_usd, expense_category, exceeded, limit_id, client_id)
VALUES ('2023-01-12T15:00:00', '0123456789', '0567845678', 0, 20000, 200, 'PRODUCT', true, 2, 1);
INSERT INTO public.transaction (created, account_from, account_to, amount_kzt, amount_rub, amount_usd, expense_category, exceeded, limit_id, client_id)
VALUES ('2023-01-12T15:00:00', '0123456789', '0567845678', 0, 10000, 100, 'PRODUCT', true, 2, 1);
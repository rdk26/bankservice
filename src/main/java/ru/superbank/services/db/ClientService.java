package ru.superbank.services.db;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.superbank.model.db.Client;
import ru.superbank.repositories.ClientRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ClientService {

    private final ClientRepository clientRepository;

    public Optional<Client> getClientById(Long id) {
        return clientRepository.getClientById(id);
    }
}

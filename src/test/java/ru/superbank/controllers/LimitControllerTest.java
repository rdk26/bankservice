package ru.superbank.controllers;

import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import ru.superbank.AbstractTest;
import ru.superbank.model.dto.request.ClientLimitRq;
import ru.superbank.model.enums.ExpenseCategory;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(value = "/data/sql/controller/limitController/ClientLimitInit.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/data/sql/controller/limitController/ClientLimitClear.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class LimitControllerTest extends AbstractTest {

    @Test
    public void getPageWithCorrectIdAndWithoutParametersIsSuccess() throws Exception {

        mockMvc.perform(get("/api/limits/v1/client/{id}", 1L))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()", Is.is(2)))
                .andExpect(jsonPath("$.content[0].created", Is.is("2023-09-01T15:00:00")))
                .andExpect(jsonPath("$.content[0].sum", Is.is(3000.0)))
                .andExpect(jsonPath("$.content[0].expenseCategory", Is.is(ExpenseCategory.PRODUCT.name())));
    }

    @Test
    public void getPageWithIncorrectIdAndWithoutParametersIsFailed() throws Exception {

        mockMvc.perform(get("/api/limits/v1/client/{id}", 10L))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.code", Is.is(404)))
                .andExpect(jsonPath("$.httpStatus", Is.is("NOT_FOUND")))
                .andExpect(jsonPath("$.text", Is.is("You entered incorrect data (id client), try again or contact support")));
    }

    @Test
    public void getPageWithCorrectIdAndCorrectCreatedRangeIsSuccess() throws Exception{

        mockMvc.perform(get("/api/limits/v1/client/{id}?createdFrom={createdFrom}&createdTo={createdTo}",
                1L, "2023-09-01T15:00:00", "2023-09-02T15:00:00"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()", Is.is(2)))
                .andExpect(jsonPath("$.content[1].created", Is.is("2023-09-02T15:00:00")))
                .andExpect(jsonPath("$.content[1].sum", Is.is(5000.0)))
                .andExpect(jsonPath("$.content[1].expenseCategory", Is.is(ExpenseCategory.SERVICE.name())));
    }

    @Test
    public void getPageWithCorrectIdAndCorrectCreatedFromIsSuccess() throws Exception{

        mockMvc.perform(get("/api/limits/v1/client/{id}?createdFrom={createdFrom}",
                        1L, "2023-09-02T15:00:00"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()", Is.is(1)))
                .andExpect(jsonPath("$.content[0].created", Is.is("2023-09-02T15:00:00")))
                .andExpect(jsonPath("$.content[0].sum", Is.is(5000.0)))
                .andExpect(jsonPath("$.content[0].expenseCategory", Is.is(ExpenseCategory.SERVICE.name())));
    }

    @Test
    public void getPageWithCorrectIdAndCorrectCreatedToIsSuccess() throws Exception{

        mockMvc.perform(get("/api/limits/v1/client/{id}?createdTo={createdTo}",
                        1L, "2023-09-01T15:00:00"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()", Is.is(1)))
                .andExpect(jsonPath("$.content[0].created", Is.is("2023-09-01T15:00:00")))
                .andExpect(jsonPath("$.content[0].sum", Is.is(3000.0)))
                .andExpect(jsonPath("$.content[0].expenseCategory", Is.is(ExpenseCategory.PRODUCT.name())));
    }

    @Test
    public void getPageWithCorrectIdAndOutsideCreatedRangeIsEmpty() throws Exception {

        mockMvc.perform(get("/api/limits/v1/client/{id}?createdFrom={createdFrom}&createdTo={createdTo}",
                1L, "2022-09-01T15:00:00", "2022-09-02T15:00:00"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()", Is.is(0)));
    }

    @Test
    public void postClientLimitWithCorrectDataIsSuccess() throws Exception {
        ClientLimitRq clientLimitRq = new ClientLimitRq(BigDecimal.valueOf(3000L), "PRODUCT");
        mockMvc.perform(post("/api/limits/v1/client/{id}", 1L)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(clientLimitRq)))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void postClientLimitWithoutSumIsFailed() throws Exception {
        ClientLimitRq clientLimitRq = new ClientLimitRq();
        clientLimitRq.setExpenseCategory("PRODUCT");
        mockMvc.perform(post("/api/limits/v1/client/{id}", 1L)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(clientLimitRq)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.code", Is.is(404)))
                .andExpect(jsonPath("$.httpStatus", Is.is("NOT_FOUND")))
                .andExpect(jsonPath("$.text", Is.is("the sum of the limit is not specified")));
    }

    @Test
    public void postClientLimitWithoutExpenseCategoryIsFailed() throws Exception {
        ClientLimitRq clientLimitRq = new ClientLimitRq();
        clientLimitRq.setSum(BigDecimal.valueOf(2000));
        mockMvc.perform(post("/api/limits/v1/client/{id}", 1L)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(clientLimitRq)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.code", Is.is(404)))
                .andExpect(jsonPath("$.httpStatus", Is.is("NOT_FOUND")))
                .andExpect(jsonPath("$.text", Is.is("the category of the limit is not specified")));
    }

    @Test
    public void postClientLimitWithSumZeroIsFailed() throws Exception {
        ClientLimitRq clientLimitRq = new ClientLimitRq();
        clientLimitRq.setSum(BigDecimal.ZERO);
        clientLimitRq.setExpenseCategory("PRODUCT");
        mockMvc.perform(post("/api/limits/v1/client/{id}", 1L)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(clientLimitRq)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.code", Is.is(404)))
                .andExpect(jsonPath("$.httpStatus", Is.is("NOT_FOUND")))
                .andExpect(jsonPath("$.text", Is.is("You entered incorrect data (sum of the limit), try again or contact support")));
    }

    @Test
    public void postClientLimitWithSumLessZeroIsFailed() throws Exception {
        ClientLimitRq clientLimitRq = new ClientLimitRq();
        clientLimitRq.setSum(BigDecimal.valueOf(-2000));
        clientLimitRq.setExpenseCategory("PRODUCT");
        mockMvc.perform(post("/api/limits/v1/client/{id}", 1L)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(clientLimitRq)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.code", Is.is(404)))
                .andExpect(jsonPath("$.httpStatus", Is.is("NOT_FOUND")))
                .andExpect(jsonPath("$.text", Is.is("You entered incorrect data (sum of the limit), try again or contact support")));
    }

    @Test
    public void postClientLimitWithIncorrectCategoryIsFailed() throws Exception {
        ClientLimitRq clientLimitRq = new ClientLimitRq();
        clientLimitRq.setSum(BigDecimal.valueOf(2000));
        clientLimitRq.setExpenseCategory("product");
        mockMvc.perform(post("/api/limits/v1/client/{id}", 1L)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(clientLimitRq)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.code", Is.is(404)))
                .andExpect(jsonPath("$.httpStatus", Is.is("NOT_FOUND")))
                .andExpect(jsonPath("$.text", Is.is("You entered incorrect data (name category), try again or contact support")));
    }
}
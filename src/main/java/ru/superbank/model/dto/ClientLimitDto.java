package ru.superbank.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class ClientLimitDto {

    private LocalDateTime created;
    private BigDecimal sum;
    private BigDecimal remaining;
    private String expenseCategory;

}

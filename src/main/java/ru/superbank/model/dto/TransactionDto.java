package ru.superbank.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class TransactionDto {

    private LocalDateTime created;

    private String accountFrom;

    private String accountTo;

    private BigDecimal amountKZT;

    private BigDecimal amountRUB;

    private BigDecimal amountUSD;

    private String expenseCategory;

    private boolean exceeded;
}

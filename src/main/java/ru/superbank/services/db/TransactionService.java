package ru.superbank.services.db;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.superbank.mappers.TransactionMapper;
import ru.superbank.model.db.Client;
import ru.superbank.model.db.ClientLimit;
import ru.superbank.model.db.Conversion;
import ru.superbank.model.db.Transaction;
import ru.superbank.model.dto.TransactionDto;
import ru.superbank.model.dto.request.TransactionRq;
import ru.superbank.repositories.TransactionRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final LimitService limitService;
    private final ConversionService conversionService;
    private final TransactionMapper transactionMapper;

    @Transactional(readOnly = true)
    public Page<TransactionDto> findAllByClientIdAndCreatedRange(Long id,
                                                                 LocalDateTime createdFrom,
                                                                 LocalDateTime createdTo,
                                                                 Pageable pageable) {
        return transactionRepository.findAllByClientAndCreatedRange(id, createdFrom, createdTo, pageable);
    }

    @Transactional(readOnly = true)
    public BigDecimal getSumByClientIdAndExpenseCategory(Long id, String expenseCategory) {
       return transactionRepository.getSumByClientIdAndExpenseCategory(id, expenseCategory).orElse(BigDecimal.ZERO);
    }

    @Transactional(readOnly = true)
    public Page<Transaction> getExceededTransactionByClientId(Long id, Pageable pageable) {
        return transactionRepository.getExceededTransactionByClientId(id, pageable);
    }

    public Transaction addTransaction(TransactionRq transactionRq, Client client) {

        BigDecimal sumRUB = transactionRq.getAmountRUB();
        BigDecimal sumKZT = transactionRq.getAmountKZT();
        BigDecimal sumUSD = BigDecimal.ZERO;

        if (sumRUB != null && sumRUB.compareTo(BigDecimal.ZERO) > 0 ) {
            sumUSD = convertCurrency(sumRUB, "USD/RUB");
        } else if (sumKZT != null && sumKZT.compareTo(BigDecimal.ZERO) > 0 ) {
            sumUSD = convertCurrency(sumKZT, "USD/KZT");
        }

        ClientLimit clientLimit = limitService.findFirstByClientAndAndExpenseCategoryOrderByCreatedDesc(client.getId(), transactionRq.getExpenseCategory());

        Transaction transaction = transactionMapper.toEntity(transactionRq);
        transaction.setClient(client);
        transaction.setAmountUSD(sumUSD);

        BigDecimal remaining = clientLimit != null? clientLimit.getRemaining() : BigDecimal.ZERO;
        BigDecimal newRemaining = remaining.subtract(sumUSD);
        boolean isExceeded = newRemaining.compareTo(BigDecimal.ZERO) < 0;

        if (clientLimit != null) {
            transaction.setLimit(clientLimit);
            clientLimit.setRemaining(newRemaining);
            limitService.saveLimit(clientLimit);
        }

        transaction.setExceeded(isExceeded);
        return transactionRepository.save(transaction);
    }

    private BigDecimal convertCurrency(BigDecimal amount, String conversionSymbol) {
        Optional<Conversion> optionalConversion = conversionService.findFirstBySymbolOrderByMadeAtDesc(conversionSymbol);
        BigDecimal rate = BigDecimal.ZERO;
        if (optionalConversion.isPresent()) {
            rate = optionalConversion.get().getRate();
        }
        return rate.compareTo(BigDecimal.ZERO) > 0 ? amount.divide(rate, RoundingMode.HALF_DOWN) : BigDecimal.ZERO;
    }
}

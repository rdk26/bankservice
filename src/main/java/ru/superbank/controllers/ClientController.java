package ru.superbank.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.superbank.exceptions.IncorrectDataException;
import ru.superbank.model.db.Client;
import ru.superbank.services.db.ClientService;

import java.util.Optional;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;
    @GetMapping("/v1/clients/{id}")
    public ResponseEntity<Client> getClientById(@PathVariable Long id) {
        Optional<Client> clientOptional = clientService.getClientById(id);
        if (clientOptional.isEmpty()) {
            throw new IncorrectDataException("You entered incorrect data, try again or contact support");
        }
        return ResponseEntity.ok(clientService.getClientById(id).get());
    }
}

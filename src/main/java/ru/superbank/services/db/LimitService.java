package ru.superbank.services.db;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.superbank.model.db.ClientLimit;
import ru.superbank.model.dto.ClientLimitDto;
import ru.superbank.repositories.LimitRepository;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class LimitService {

    private final LimitRepository limitRepository;

    @Transactional(readOnly = true)
    public ClientLimit findFirstByClientAndAndExpenseCategoryOrderByCreatedDesc(Long clientId, String expenseCategory) {
        return limitRepository.findFirstByClientIdAndExpenseCategoryOrderByCreatedDesc(clientId, expenseCategory);
    }

    @Transactional(readOnly = true)
    public Page<ClientLimitDto> findAllByClientIdAndCreatedRange(Long id, LocalDateTime createdFrom,
                                                               LocalDateTime createdTo, Pageable pageable) {
        return limitRepository.findAllByClientIdAndCreatedRange(id, createdFrom, createdTo, pageable);
    }

    public ClientLimit saveLimit(ClientLimit clientLimit) {
        return limitRepository.save(clientLimit);
    }
}

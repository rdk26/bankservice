package ru.superbank.model.enums;

public enum ExpenseCategory {
    PRODUCT,
    SERVICE
}

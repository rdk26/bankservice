package ru.superbank.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.superbank.exceptions.IncorrectDataException;
import ru.superbank.exceptions.NotFoundLimitException;
import ru.superbank.model.db.Client;
import ru.superbank.model.db.ClientLimit;
import ru.superbank.model.dto.ClientLimitDto;
import ru.superbank.model.dto.request.ClientLimitRq;
import ru.superbank.model.enums.ExpenseCategory;
import ru.superbank.services.db.ClientService;
import ru.superbank.services.db.LimitService;
import ru.superbank.services.db.TransactionService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import static ru.superbank.enums.ErrorMessages.EMPTY_DATA;
import static ru.superbank.enums.ErrorMessages.INCORRECT_DATA_MESSAGE;

@RestController
@RequestMapping("/api/limits")
@AllArgsConstructor
@Tag(name = "Лимиты клиента")
public class LimitController {
    private final LimitService limitService;
    private final ClientService clientService;
    private final TransactionService transactionService;
    @GetMapping("/v1/client/{id}")
    @Operation(
            operationId = "getLimits",
            description = "Получение списка лимитов",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Успешное получение списка лимитов клиента",
                          content = {@Content(mediaType = "application/json",
                                  array = @ArraySchema(schema = @Schema(implementation = ClientLimitDto.class, title = "Лимит клиента")))}),
                    @ApiResponse(responseCode = "400", description = "Некорректные входные параметры",
                          content = {@Content(mediaType = "application/json",
                                  schema = @Schema(implementation = String.class),
                                  examples = {@ExampleObject(value = "You entered incorrect data, try again or contact support")})})
            })
    public ResponseEntity<Page<ClientLimitDto>> getLimitsByClientIdAndCreatedRange(@PathVariable Long id,
                                                                                   @RequestParam(name = "createdFrom", required = false)
                                                                                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime createdFrom,
                                                                                   @RequestParam(name = "createdTo", required = false)
                                                                                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime createdTo,
                                                                                   @ParameterObject Pageable pageable)  {
        checkClientId(id);
        return ResponseEntity.ok(limitService.findAllByClientIdAndCreatedRange(id, createdFrom, createdTo, pageable));
    }

    @GetMapping("/v1/client/{id}/{category}")
    @Operation(
            operationId = "getLimitsByCategory",
            description = "Получение списка лимитов по категории",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Успешное получение списка лимитов клиента по категории",
                            content = {@Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ClientLimitDto.class)))}),
                    @ApiResponse(responseCode = "400", description = "Некорректные входные параметры",
                            content = {@Content(mediaType = "application/json",
                                    schema = @Schema(implementation = String.class),
                                    examples = {@ExampleObject(value = "You entered incorrect data, try again or contact support")})})
            })
    public ResponseEntity<ClientLimit> getLimitByExpenseCategory(@PathVariable Long id,
                                                                 @PathVariable String category) {
        checkClientId(id);
        ClientLimit clientLimit = limitService.findFirstByClientAndAndExpenseCategoryOrderByCreatedDesc(id, category);
        if (clientLimit == null) {
            throw new NotFoundLimitException("Limit for this category was not found");
        }
        return ResponseEntity.ok(clientLimit);
    }

    @PostMapping("/v1/client/{id}")
    @Operation(
            operationId = "getLimits",
            description = "Добавление нового лимитов",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Успешное получение добавление клиента",
                            content = {@Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = ClientLimitDto.class, title = "Лимит клиента")))}),
                    @ApiResponse(responseCode = "400", description = "Некорректные входные параметры",
                            content = {@Content(mediaType = "application/json",
                                    schema = @Schema(implementation = String.class),
                                    examples = {@ExampleObject(value = "You entered incorrect data, try again or contact support")})})
            })
    public ResponseEntity<ClientLimit> addNewLimit(@RequestBody ClientLimitRq clientLimitRq, @PathVariable Long id) {
        Client client = checkClientId(id);
        String category = clientLimitRq.getExpenseCategory();
        if (category == null) {
            throw new IncorrectDataException(String.format(EMPTY_DATA.value(), "category of the limit"));
        }
        BigDecimal sum = clientLimitRq.getSum();
        if (sum == null) {
            throw new IncorrectDataException(String.format(EMPTY_DATA.value(), "sum of the limit"));
        }
        boolean categoryExists = Arrays.stream(ExpenseCategory.values()).anyMatch(value -> category.equals(value.name()));
        if (!categoryExists) {
            throw new IncorrectDataException(String.format(INCORRECT_DATA_MESSAGE.value(), "name category"));
        }
        if (sum.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IncorrectDataException(String.format(INCORRECT_DATA_MESSAGE.value(), "sum of the limit"));
        }
        ClientLimit clientLimit = new ClientLimit();
        clientLimit.setClient(client);
        clientLimit.setSum(sum);
        BigDecimal sumOfTransactionByCategory = transactionService.getSumByClientIdAndExpenseCategory(id, category);
        clientLimit.setRemaining(sum.subtract(sumOfTransactionByCategory));
        clientLimit.setExpenseCategory(category);
        ClientLimit savedLimit = limitService.saveLimit(clientLimit);
        return ResponseEntity.ok(savedLimit);
    }

    private Client checkClientId(Long id) {
        Optional<Client> client = clientService.getClientById(id);
        if (client.isEmpty()) {
            throw new IncorrectDataException(String.format(INCORRECT_DATA_MESSAGE.value(), "id client"));
        }
        return client.get();
    }
}

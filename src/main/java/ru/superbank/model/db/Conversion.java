package ru.superbank.model.db;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@ToString
@Table(uniqueConstraints = {
        @UniqueConstraint(name = "uniqueConversionMadeAtAndSymbol", columnNames = {"made_at", "symbol"})
})
public class Conversion {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "rate")
    private BigDecimal rate;

    @Column(name = "made_at")
    private LocalDate madeAt;

    @Column(name = "symbol")
    private String symbol;
}

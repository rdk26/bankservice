package ru.superbank.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.superbank.exceptions.IncorrectDataException;
import ru.superbank.model.db.Client;
import ru.superbank.model.db.Transaction;
import ru.superbank.model.dto.TransactionDto;
import ru.superbank.model.dto.request.TransactionRq;
import ru.superbank.services.db.ClientService;
import ru.superbank.services.db.TransactionService;

import java.time.LocalDateTime;
import java.util.Optional;

import static ru.superbank.enums.ErrorMessages.INCORRECT_DATA_MESSAGE;

@RestController
@RequestMapping("/api/transactions")
@AllArgsConstructor
public class TransactionController {

    private final TransactionService transactionService;
    private final ClientService clientService;

    @GetMapping("/v1/client/{id}")
    @Operation(
            operationId = "getTransactions",
            description = "Получение списка транзакций",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Успешное получение списка транзакций клиента",
                            content = {@Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = TransactionDto.class, title = "Лимит клиента")))}),
                    @ApiResponse(responseCode = "400", description = "Некорректные входные параметры",
                            content = {@Content(mediaType = "application/json",
                                    schema = @Schema(implementation = String.class),
                                    examples = {@ExampleObject(value = "You entered incorrect data, try again or contact support")})})
            })
    public ResponseEntity<Page<TransactionDto>> getTransactionsByClientIdAndCreatedRange(@PathVariable Long id,
                                                                                         @RequestParam(name = "createdFrom", required = false)
                                             @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime createdFrom,
                                                                                         @RequestParam(name = "createdTo", required = false)
                                                 @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime createdTo,
                                                                                         @ParameterObject Pageable pageable) {
        Client client = getClientById(id);
        return ResponseEntity.ok(transactionService.findAllByClientIdAndCreatedRange(id, createdFrom, createdTo, pageable));
    }

    @GetMapping("/exceeded/v1/client/{id}")
    @Operation(
            operationId = "getExceededTransactions",
            description = "Получение списка транзакций превысивших лимит",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Успешное получение списка транзакций клиента",
                            content = {@Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = TransactionDto.class, title = "Лимит клиента")))}),
                    @ApiResponse(responseCode = "400", description = "Некорректные входные параметры",
                            content = {@Content(mediaType = "application/json",
                                    schema = @Schema(implementation = String.class),
                                    examples = {@ExampleObject(value = "You entered incorrect data, try again or contact support")})})
            })
    public ResponseEntity<Page<Transaction>> getExceededTransactionByClientId(@PathVariable Long id, @ParameterObject Pageable pageable) {
        Client client = getClientById(id);
        return ResponseEntity.ok(transactionService.getExceededTransactionByClientId(id, pageable));
    }

    @PostMapping("/v1/client/{id}")
    public ResponseEntity<Transaction> addTransaction(@RequestBody TransactionRq transactionRq, @PathVariable Long id) {
        Client client = getClientById(id);
        Transaction transaction = transactionService.addTransaction(transactionRq, client);
        return ResponseEntity.ok(transaction);
    }

    private Client getClientById(Long id) {
        Optional<Client> client = clientService.getClientById(id);
        if (client.isEmpty()) {
            throw new IncorrectDataException(String.format(INCORRECT_DATA_MESSAGE.value(), "id client"));
        }
        return client.get();
    }
}

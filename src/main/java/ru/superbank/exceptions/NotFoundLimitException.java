package ru.superbank.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundLimitException extends RuntimeException {

    public NotFoundLimitException(String message) {
        super(message);
    }
}

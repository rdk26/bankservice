package ru.superbank.services;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.superbank.services.db.ConversionService;

import java.io.IOException;

@Service
public class ScheduleService {

    private final ConversionService conversionService;

    public ScheduleService(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Scheduled(cron = "0 0 6 * * ?")
    public void getExchangeRateForCurrencyPair() throws Exception {
        conversionService.getExchangeRateForCurrencyPair("USD/RUB");
        conversionService.getExchangeRateForCurrencyPair("USD/KZT");
    }
}

package ru.superbank.services.db;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import ru.superbank.model.db.Conversion;
import ru.superbank.repositories.ConversionRepository;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ConversionService {

    private final ConversionRepository conversionRepository;
    private static final String API_KEY = "bfa30ad9c67641949ab830c49ae24e41";
    private static final String BASE_URL = "https://api.twelvedata.com/exchange_rate";


    public Optional<Conversion> findFirstBySymbolOrderByMadeAtDesc(String symbol) {
        return conversionRepository.findFirstBySymbolOrderByMadeAtDesc(symbol);
    }

    public void getExchangeRateForCurrencyPair(String symbol) throws Exception {
        String response = callTwelveDataAPI(symbol);
        JSONObject jsonObject = new JSONObject(response);
        if (jsonObject.has("symbol") && jsonObject.has("rate")) {
            double rate = jsonObject.getDouble("rate");
            LocalDate madeAt = LocalDate.now();
            Conversion conversion = new Conversion();
            conversion.setRate(BigDecimal.valueOf(rate));
            conversion.setMadeAt(madeAt);
            conversion.setSymbol(symbol);
            conversionRepository.save(conversion);
        }
    }

    private String callTwelveDataAPI(String symbol) throws Exception {
        String url = BASE_URL + "?symbol=" + symbol + "&apikey=" + API_KEY;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }
}

package ru.superbank.controllers;

import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import ru.superbank.AbstractTest;
import ru.superbank.model.dto.request.TransactionRq;
import ru.superbank.model.enums.ExpenseCategory;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(value = "/data/sql/controller/transactionController/TransactionInit.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/data/sql/controller/transactionController/TransactionClear.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class TransactionControllerTest extends AbstractTest {
    @Test
    public void getPageTransactionsWithCorrectIdAndWithoutParametersIsSuccess() throws Exception {

        mockMvc.perform(get("/api/transactions/v1/client/{id}", 1L))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()", Is.is(6)))
                .andExpect(jsonPath("$.content[0].created", Is.is("2023-01-02T15:04:00")))
                .andExpect(jsonPath("$.content[0].amountRUB", Is.is(50000.0)))
                .andExpect(jsonPath("$.content[0].amountUSD", Is.is(500.0)))
                .andExpect(jsonPath("$.content[0].amountKZT", Is.is(0.0)))
                .andExpect(jsonPath("$.content[0].expenseCategory", Is.is(ExpenseCategory.PRODUCT.name())))
                .andExpect(jsonPath("$.content[0].exceeded", Is.is(false)))
                .andExpect(jsonPath("$.content[0].accountFrom", Is.is("0123456789")))
                .andExpect(jsonPath("$.content[0].accountTo", Is.is("0567845678")));
    }

    @Test
    public void getPageTransactionsWithIncorrectIdAndWithoutParametersIsFailed() throws Exception {

        mockMvc.perform(get("/api/transactions/v1/client/{id}", 10L))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.code", Is.is(404)))
                .andExpect(jsonPath("$.httpStatus", Is.is("NOT_FOUND")))
                .andExpect(jsonPath("$.text", Is.is("You entered incorrect data (id client), try again or contact support")));
    }

    @Test
    public void getPageTransactionsWithCorrectIdAndCorrectCreatedRangeIsSuccess() throws Exception{

        mockMvc.perform(get("/api/transactions/v1/client/{id}?createdFrom={createdFrom}&createdTo={createdTo}",
                        1L, "2023-01-01T15:00:00", "2023-01-03T15:00:00"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()", Is.is(2)))
                .andExpect(jsonPath("$.content[1].created", Is.is("2023-01-03T15:00:00")))
                .andExpect(jsonPath("$.content[1].amountRUB", Is.is(60000.0)))
                .andExpect(jsonPath("$.content[1].expenseCategory", Is.is(ExpenseCategory.PRODUCT.name())));
    }

    @Test
    public void getPageExceededTransactionsWithCorrectId() throws Exception {

        mockMvc.perform(get("/api/transactions/exceeded/v1/client/{id}", 1L))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()", Is.is(2)))
                .andExpect(jsonPath("$.content[0].created", Is.is("2023-01-12T15:00:00")))
                .andExpect(jsonPath("$.content[0].amountRUB", Is.is(20000.0)))
                .andExpect(jsonPath("$.content[0].amountUSD", Is.is(200.0)))
                .andExpect(jsonPath("$.content[0].amountKZT", Is.is(0.0)))
                .andExpect(jsonPath("$.content[0].expenseCategory", Is.is(ExpenseCategory.PRODUCT.name())))
                .andExpect(jsonPath("$.content[0].exceeded", Is.is(true)))
                .andExpect(jsonPath("$.content[0].accountFrom", Is.is("0123456789")))
                .andExpect(jsonPath("$.content[0].accountTo", Is.is("0567845678")));
    }

    @Test
    public void addTransactionForProductAndWriteLikeExceededAndClientHasLimit() throws Exception {
        TransactionRq transactionRq = new TransactionRq();
        transactionRq.setAmountRUB(BigDecimal.valueOf(50000));
        transactionRq.setCreated(LocalDateTime.now());
        transactionRq.setExpenseCategory(ExpenseCategory.PRODUCT.name());
        transactionRq.setAmountKZT(BigDecimal.ZERO);
        transactionRq.setAccountTo("1234567890");
        transactionRq.setAccountFrom("3214567890");
        mockMvc.perform(post("/api/transactions/v1/client/{id}", 1L)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(transactionRq)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.exceeded", Is.is(true)))
                .andExpect(jsonPath("$.amountUSD", Is.is(500)));

    }

    @Test
    public void addTransactionForServiceAndWriteLikeExceededAndClientDoesNotHaveLimit() throws Exception {
        TransactionRq transactionRq = new TransactionRq();
        transactionRq.setAmountRUB(BigDecimal.valueOf(70000));
        transactionRq.setCreated(LocalDateTime.now());
        transactionRq.setExpenseCategory(ExpenseCategory.SERVICE.name());
        transactionRq.setAmountKZT(BigDecimal.ZERO);
        transactionRq.setAccountTo("1234567890");
        transactionRq.setAccountFrom("3214567890");
        mockMvc.perform(post("/api/transactions/v1/client/{id}", 1L)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(transactionRq)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.exceeded", Is.is(true)))
                .andExpect(jsonPath("$.amountUSD", Is.is(700)));
    }

    @Test
    public void addTransactionForServiceAndWriteLikeNotExceededAndClientHasLimit() throws Exception {
        TransactionRq transactionRq = new TransactionRq();
        transactionRq.setAmountRUB(BigDecimal.valueOf(70000));
        transactionRq.setCreated(LocalDateTime.now());
        transactionRq.setExpenseCategory(ExpenseCategory.SERVICE.name());
        transactionRq.setAmountKZT(BigDecimal.ZERO);
        transactionRq.setAccountTo("1234567890");
        transactionRq.setAccountFrom("3214567890");
        mockMvc.perform(post("/api/transactions/v1/client/{id}", 3L)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(transactionRq)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.exceeded", Is.is(false)))
                .andExpect(jsonPath("$.amountUSD", Is.is(700)));
    }
}
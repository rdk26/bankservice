package ru.superbank.model.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Min;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private LocalDateTime created;

    @Column(name = "account_from", length = 10)
    private String accountFrom;

    @Column(name = "account_to", length = 10)
    private String accountTo;

    @Column(name = "amount_kzt")
    @Min(0)
    private BigDecimal amountKZT;

    @Column(name = "amount_rub")
    @Min(0)
    private BigDecimal amountRUB;

    @Column(name = "amount_usd")
    @Min(0)
    private BigDecimal amountUSD;

    @Column(name = "expense_category")
    private String expenseCategory;

    @Column
    private boolean exceeded;

    @JsonIgnore
    @EqualsAndHashCode.Exclude
    @ManyToOne(targetEntity = Client.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    private Client client;

    @JsonIgnore
    @EqualsAndHashCode.Exclude
    @ManyToOne(targetEntity = ClientLimit.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "limit_id")
    private ClientLimit limit;

}

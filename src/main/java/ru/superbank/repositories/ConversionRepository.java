package ru.superbank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.superbank.model.db.Conversion;

import java.util.Optional;

public interface ConversionRepository extends JpaRepository<Conversion, Long> {
    Optional<Conversion> findFirstBySymbolOrderByMadeAtDesc(String symbol);
}

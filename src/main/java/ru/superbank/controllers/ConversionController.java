package ru.superbank.controllers;


import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.superbank.services.db.ConversionService;

@RestController
@RequestMapping("/api/v1/conversion")
@AllArgsConstructor
public class ConversionController {

    private final ConversionService conversionService;

    @PostMapping
    public void addConversion(@RequestParam(name = "symbol") String symbol) throws Exception {
        conversionService.getExchangeRateForCurrencyPair(symbol);
    }
}

package ru.superbank.services.db;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ru.superbank.AbstractTest;
import ru.superbank.model.db.Conversion;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;


@Sql(value = "/data/sql/controller/transactionController/TransactionInit.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "/data/sql/controller/transactionController/TransactionClear.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class ConversionServiceTest extends AbstractTest {

    @Autowired
    private ConversionService conversionService;

    @Test
    void getRateForUSDRUBisShouldBeEquals100() {
        Optional<Conversion> optionalConversion = conversionService.findFirstBySymbolOrderByMadeAtDesc("USD/RUB");
        Conversion conversion = optionalConversion.orElse(new Conversion());
        assertEquals(0, BigDecimal.valueOf(100.00).compareTo(conversion.getRate()));
    }

    @Test
    void getRateForUSDKZTisShouldBeEquals470() {
        Optional<Conversion> optionalConversion = conversionService.findFirstBySymbolOrderByMadeAtDesc("USD/KZT");
        Conversion conversion = optionalConversion.orElse(new Conversion());
        assertEquals(0, BigDecimal.valueOf(470.00).compareTo(conversion.getRate()));
    }
}
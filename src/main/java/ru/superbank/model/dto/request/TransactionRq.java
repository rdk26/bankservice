package ru.superbank.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class TransactionRq {
    private LocalDateTime created;

    private String accountFrom;

    private String accountTo;

    private BigDecimal amountKZT;

    private BigDecimal amountRUB;

    private String expenseCategory;
}
